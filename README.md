# Forena
Forena is an MVC Symfony Framework for rapidly building web applications.  It was inspired
by a report writer developed for Drupal under the same name, but is being rewritten as a Symfony component 
for use in the broader community.

The current production version of the project is implemented as a module in the 
Drupal content management system.  Work is underway to port this into an application
independent framework which would still be used under drupal be also be 
available to the broader PHP community. 

#Warning - under construction
This software is currently under active construction and is not suitable for production use. 

# Installation
Install the library via composer. 

````
composer require forena/framework
````

#Using the Forena
For more details regarding the use of the forena framework, please see
See project [[ forena/forena ]](http://gitlab.com/forena/forena) for details 
about using forena templating components directly in your code. 